<details>
<summary>New Team Member</summary>

#### General Section 

* [ ] In the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1YF4mtTAFmH1E7HqZASFTitGQUZubd12wsLDhzloEz3I/edit). 
* [ ] Read about the GitLab Sales Learning Framework listed in the [sales training handbook](https://about.gitlab.com/handbook/sales-training/).
* [ ] Review the [Sales Compensation Plan](https://internal.gitlab.com/handbook/sales/sales-commission/#fy23-sales-commission-policies).
* [ ] I have read through the [SA Architects Processes](https://about.gitlab.com/handbook/customer-success/solutions-architects/processes/) and the [SA Engagement Model](https://about.gitlab.com/handbook/customer-success/solutions-architects/processes/#solution-architect-engagement-models) for my region. 
* [ ] I have access to Opportunities in SFDC. If not, I will submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new) for [the role that coincides with my team members](https://gitlab.my.salesforce.com/00O8X000008RSjQ).


#### Getting Good At SA Processes

* [ ] I have [associated my email to SalesForce](https://about.gitlab.com/handbook/sales/prospect-engagement-best-practices). This will ensure that correspondence with a customer via email is recorded within the opportunity in SalesForce.
* [ ] I have completed my [TeamOps Certification](https://levelup.gitlab.com/learn/course/teamops) (formerly known as MECC)
* [ ] I have _attended_ a GitLab Group Conversation.
* [ ] I have _participated_ in a GitLab Group Conversation by reading the Key Slides, Watching the Prep Video (if available), and asking a question in the Group Conversation.
* [ ] I have read and understand my responsibility to conduct [Technical Discovery on opportunities](https://about.gitlab.com/handbook/customer-success/solutions-architects/processes/technical-discovery/) and bring that customer intelligence into GitLab.
* [ ] Collaborate by [Riding Along](https://about.gitlab.com/handbook/customer-success/solutions-architects/sa-practices/ride-alongs/) practise and participate actively across regions and segements in ride alongs 
* [ ] I have successfully completed the [Values Stream Assessment Accreditation](https://levelup.gitlab.com/learn/learning-path/value-stream-assessment-accreditation)
* [ ] I have pitched a Value Stream Assessment to a customer and have provided a link to the chorus call below
* [ ] I have delivered an in-person Value Stream Assessment session with a customer and have provided its collateral below
* [ ] I have read and understand my responsibility of [transitioning accounts from pre-sales to post-sales](https://about.gitlab.com/handbook/customer-success/pre-sales-post-sales-transition/#internal-account-team-meeting)
* [ ] I have read and understand how to follow the [Request for Proposal (RFP) process](https://about.gitlab.com/handbook/security/security-assurance/field-security/Field-Security-RFP.html)
* [ ] I have read and understand how to provide [customer feedback to product](https://about.gitlab.com/handbook/product/product-processes/customer-issues-prioritization-framework/#framework-inputs-and-outputs)

#### Getting Good At Understanding The Product

* [ ] Please login to Level Up - (GitLab’s Learning Experience Platform) and begin working through your role based onboarding Journey “[Solutions Architect: 30-60-90 Onboarding Journey](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/solutions-architect-30-60-90-onboarding-journey)”. This Learning Path is designed to accelerate your time to productivity by focusing on the key sales-specific elements you need to know and be able to do within the first three months at GitLab
* [ ] I have taken the [GitLab with Git Essentials](https://levelup.gitlab.com/courses/gitlab-with-git-essentials) self-paced training.
* [ ] I have successfully completed the [GitLab Certified Associate](https://levelup.gitlab.com/learning-paths/certified-git-associate-learning-path) learning pathway. ⚡️Action: Add screenshot of completion badge to comments and discuss with mentor/leader to celebrate.
* [ ] I have successfully completed the [CI/CD with GitLab](https://levelup.gitlab.com/learning-paths/certified-ci-cd-specialist-learning-path) learning pathway. ⚡️Action: Add screenshot of completion badge to comments and discuss with mentor/leader to celebrate.
* [ ] I have successfully completed the [GitLab Certified Security Specialist](https://levelup.gitlab.com/learning-paths/certified-security-specialist-learning-path) learning pathway. ⚡️Action: Add screenshot of completion badge to comments and discuss with mentor/leader to celebrate.
* [ ] I have successfully completed the [GitLab Certified Project Management Associate](https://levelup.gitlab.com/learning-paths/gitlab-project-management-associate-learning-path) learning pathway.⚡️Action: Add screenshot of completion badge to comments and discuss with mentor/leader to celebrate.

#### Getting Good At Helping Asynchronously

* [ ] I have added [Slack channels](https://handbook.gitlab.com/handbook/communication/chat/#channels):  #solutions-architects and #cs-questions to my Slack.
* [ ] I have reviewed [Stack Overflow for Teams](https://about.gitlab.com/handbook/customer-success/solutions-architects/tools-and-resources/#stack-overflow-for-teams). 
* [ ] I have joined [Customer Success Stack Overflow.](https://stackoverflowteams.com/c/gitlab-customer-success/).
* [ ] I have provided an answer to a question from Slack or [Customer Success Stack Overflow.](https://stackoverflowteams.com/c/gitlab-customer-success/). ⚡️Action: Add [link or screenshot to your user page](https://stackoverflowteams.com/c/gitlab-customer-success/users) and review with leader to receive credit.
* [ ] I have provided an answer to five questions. 
⚡️Action: Add [link or screenshot to your user page](https://stackoverflowteams.com/c/gitlab-customer-success/users) and review with leader to receive credit.
* [ ] I have provided an answer to ten questions. 
⚡️Action: Add [link or screenshot to your user page](https://stackoverflowteams.com/c/gitlab-customer-success/users) and review with leader to receive credit.
* [ ] I have participated as a shadow. Q&A Leader or Presenter for a [Field Marketing Workshop](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/5293). 
⚡️Action: Please provide a quick summary in comments below for discussion with mentor or leader.

#### Getting Good at Taking Time Off

GitLab takes time off seriously. Time away from work can be extremely helpful for maintaining a [healthy work/life balance](https://about.gitlab.com/company/culture/all-remote/people/#worklife-harmony). GitLab is [family and friends first, work second](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second).

* [ ] I have added all of the [Family & Friends Days](https://about.gitlab.com/company/family-and-friends-day/) for the year into Time Off by Deel in Slack.
* [ ] I have scheduled at least 1 vacation day in the first 3 months.
* [ ] I have setup my out of office in Google Mail. 


#### Getting Cloud Ready

* [ ] I am setup with [GitLab Demo Cloud](https://gitlabdemo.cloud/login)
* [ ] I have used [GitLab Sandbox Cloud](https://gitlabsandbox.cloud/cloud) to create [an individual AWS account and GCP project](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#how-to-get-started).
* [ ] I have successfully leveraged [OpenID Connect (OIDC)](https://docs.gitlab.com/ee/ci/cloud_services/) to [retrieve credentials from AWS within a CI Pipeline](https://docs.gitlab.com/ee/ci/cloud_services/aws) and have left a link to the pipeline in a comment below.
------------

Helpful Information
> * 📹  [Walkthrough Video](https://youtu.be/7PvTjYTkYTo) <BR>
> * [Guided Exploration - Configure OpenID Connect Between GitLab and AWS](https://gitlab.com/guided-explorations/aws/configure-openid-connect-in-aws)

------------
* [ ] I have successfully leveraged [OIDC](https://docs.gitlab.com/ee/ci/cloud_services/) to [retrieve credentials from Google within a CI Pipeline](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud) and have left a link to the pipeline in a comment below.
------------
Helpful Information
> * 📹 [Walkthrough video](https://www.youtube.com/watch?v=Psfy3dIa6w8) <BR>
>* [Guided Exploration - Configure OpenID Connect in Google Cloud](https://gitlab.com/guided-explorations/gcp/)


------------
* [ ] I have provisioned an AWS or GCP Kubernetes Cluster in the [GitLab Demo Cloud](https://gitlabdemo.cloud) space [following this Simple Kubernetes Management tutorial](https://about.gitlab.com/blog/2022/11/15/simple-kubernetes-management-with-gitlab/) and have left a link to the cluster management pipeline in a comment below.

#### Getting Demo Ready

* [ ] I have became familiar with h[ow to use demo projects in CS Shared Demo Space](https://gitlab.com/gitlab-learn-labs/webinars/how-to-use-these-projects).
* [ ] I have reviewed [Weekly Office Hours](https://docs.google.com/document/d/1u8e-iElGfXvVquIAgJwRO6hwWTgd0hk7UBkDOD-8zQc/edit) to learn about enhancements or questions to the Demo Space. 
* [ ] I have created a licensed demo group by [creating an Access Request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new), using the *GitlabCom_Licensed_Demo_Group_Request* template.
* [ ] Follow the steps outlined in the [Demo Systems Initial Set Up](https://gitlab.com/gitlab-com/customer-success/demo-engineering/demo-systems-initial-set-up) issue, to set up your demo environment. Please note: office hours are available via the calendar invite link in the issue.

--------
 **Platform Demo Guide**

* [ ] I reviewed the [Platform Demo Guide](https://gitlab.com/gitlab-com/customer-success/solutions-architecture/platform-demo-guide)

**End-to-End Ultimate, Security Demo**

* [ ] I have prepared my own script for the demo and have successfully gone through a dry-run
* [ ] I have successfully demoed this in front of a customer

**Portfolio Management Demo (including analytics)**

* [ ] I have prepared my own script for the demo and have successfully gone through a dry-run
* [ ] I have successfully demoed this in front of a customer

**GitOps Workflow Demo** ([Push-based and Pull-based](https://about.gitlab.com/blog/2021/04/27/gitops-done-3-ways/))

* [ ] I have prepared my own script for the demo and have successfully gone through a dry-run
* [ ] I have successfully demoed this in front of a customer

**Compliance Premium Demo** (Push-based and Pull-based)

* [ ] I have prepared my own script for the demo and have successfully gone through a dry-run
* [ ] I have successfully demoed this in front of a customer

#### Getting Good With GitLab Reference Architectures

> [GitLab has a variety of installations](https://docs.gitlab.com/ee/administration/reference_architectures/) and it is important to be skilled at each of them.

* [ ] I have reviewed the [reference architecture documentation](https://docs.gitlab.com/ee/administration/reference_architectures/).
* [ ] I have successfully installed GitLab via Omnibus ([video of process](https://www.youtube.com/watch?v=7hwliqwJPi0)) and provided a picture of that installation below.
* [ ] I have successfully installed GitLab via the Helm Chart ([video of process](https://www.youtube.com/watch?v=3XRE-sCT9H0)) and provided a picture of that installation below.


#### Getting Good At Understanding Our Competition

* [ ] I have taken the course [Competing Effectively](https://levelup.gitlab.com/courses/competing-with-github-toolkit). ⚡️Action: Please provide a screenshot of completion of course and attach to issue below.
* [ ] I have read the battle card for [GitHub](https://app.crayon.co/act/gitlab/battlecard/43319/?sales-engagement-preview-mode=true).
* [ ] I have read the battle card for [Snyk](https://app.crayon.co/act/gitlab/battlecard/42274/?sales-engagement-preview-mode=true).
* [ ] I have read the battle card for [Jenkins](https://app.crayon.co/act/gitlab/battlecard/42688/?sales-engagement-preview-mode=true).
* [ ] I have read the battle card for [Microsoft Azure DevOps (ADO)](https://app.crayon.co/act/gitlab/battlecard/42699/?sales-engagement-preview-mode=true).
* [ ] I have read the battle card for [Jira Software.](https://app.crayon.co/act/gitlab/battlecard/45296/?sales-engagement-preview-mode=true)
* [ ] I have reviewed the [Second Tier Competitors](https://app.crayon.co/intel/gitlab/competitors/). 
* [ ] I have joined the #competition slack channel to keep up to date with the competition. 
  

#### Getting Good At Pitching Professional Services

* [ ] I have gone through the [Positioning Professional Services](https://levelup.gitlab.com/courses/positioning-professional-services) training
* [ ] I have read and understand my responsibility of [engaging Professional Services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/working-with/#for-sales-reps-and-sas-how-to-order-professional-services) in the sales process
* [ ] I have successfully pitched a professional services offering all the way through the PS Quoting process
* [ ] I have worked with a partner to position professional services through a third-party


#### Getting Good At Customer Engagements

* [ ] I have reviewed [Marketing Support Requests](https://gitlab.com/gitlab-com/customer-success/solutions-architecture/marketing-support-requests/-/issues) to see if I can participate in an in-person events.
* [ ] I understand that I need to request approval from my leadership and provide visibility into field engagements.

#### Getting Good At Feedback
* [ ] I have completed at least 75% of the SA focused part of this onboarding issue and provided feedback in [this task](https://gitlab.com/gitlab-com/customer-success/solutions-architecture-leaders/sa-initiatives/-/issues/265?work_item_iid=300). 
* [ ] I have reviewed the SA focused part of this onboarding issue with my manager. 

</details>
